/*Hello world,
dieses kleine Javaskript umrandet im Web Websiten anhand von Ampelfarben.
Das Programm ist opensource und die Listen von guten Vorbildern (grün)
und von vermeidbaren Seiten (orange) kann laufend ergänzt werden.
Seiten, die noch nicht kategorisiert sind erscheinen grau.
Eine kleine Hilfestellung für unseren Weg nach Utopia.
(Programmiert von jen_mary_kay)*/

/*Das Programm funktioniert nach folgendem Schema
(unten im Anhang findet ihr die enstprechenden Listen)*/

/*führe das Programm aus, d.h. Umrande alle URLS grau*/
document.body.style.border = "50px solid grey";
/*außer, die URL enthält das Wort com*/
URL = window.location.hostname;
  if (URL.includes("com"))
/*(wenn die URL "com" enthält, dann nimm bitte orange*/
  {
    document.body.style.border = "50px solid orange";
  }
/*(wenn die URL "org" enthält, dann nimm bitte grün*/
  else if (URL.includes("org"))
  {
    document.body.style.border = "50px solid green";
  }
  /*(wenn die URL "de" enthält, dann nimm bitte gelb*/
  else if (URL.includes("de"))
  {
    document.body.style.border = "50px solid yellow";
  }

/*Außnahmen (AUSSER)*/

  /*

  ANHANG:

  Greylist = alle URLs (undefiniert, ohne Ampel)
  */

  /*
  Orangelist = "böse" Seiten (amazon, google)
  -> für eine sozialökologisch und gemeinwohlorientierte Zukunft sollten diese Seiten,
  wenn möglich eher vermieden werden.
  */
  Orangelist = ['amazon','google', 'facebook','whatapp', 'instagram'];
  /* endlos Liste */
    /* wenn auf der Orangeliste amazon steht, bitte orange machen*/
    for (var i=0;i<Orangelist.length;i++)
  {
    if (URL.includes(Orangelist[i]))
  {
    document.body.style.border = "50px solid orange";
  }
  }

  /*
  Yellowlist = neutrale Seiten (telegram, Signal)
  -> z.B. nicetohave, Vorbilder, schöne Projekte
  */
  Yellowlist = ['telegram','signal', 'nebenan'];
  /* endlos Liste */
    /* wenn auf der Yellowliste z.B. telegram steht, bitte gelb machen*/
    for (var i=0;i<Yellowlist.length;i++)
  {
    if (URL.includes(Yellowlist[i]))
  {
    document.body.style.border = "50px solid yellow";
  }
  }

  /*
  Greenlist = "gute" Seiten (m4h, kartevonmorgen)
  -> z.B. musthaves, Projekte mit SINN (sozial-inklusiv-nachhaltig-nachbarschaftlich),
  gute Vorbilder, sozialökologische Projekte für eine nachhaltige Zukunft,
  Gemeinwohlökonomie
  */
  Greenlist = ['Utopia','m4h','kartevonmorgen', 'Gemeinwohl', 'greenpeace'];
  /* endlos Liste */
    /* wenn auf der Greenlist z.B. m4h steht, bitte grün machen*/
    for (var i=0;i<Greenlist.length;i++)
  {
    if (URL.includes(Greenlist[i]))
  {
    document.body.style.border = "50px solid green";
  }
  }
